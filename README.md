# Trabalho Final 2021/2

Este é o trabalho final da disciplina de Fundamentos de Sistemas Embarcados (2021/2). O trabalho final pode ser feito em dupla ou individualmente.

## 1. Objetivos

O objetivo deste trabalho é criar um sistema distribuído de Automação Residencial utilizando um computador (PC) como sistema computacional central e microcontroladores ESP32 como dispositivos distribuídos, interconectados via Wifi através do protocolo MQTT.

![Figura](/imagens/diagrama_arquitetura.png)

## 2. Componentes do Sistema

O sistema do **Servidor Central** será composto por:
1. PC conectado à rede (Wifi | Ethernet);
2. Saída de Som para Alarme.

**Cliente distribuído - Energia**:
1. Dev Kit ESP32;
2. Sensor de Temperatura e Umidade DHT11;
3. Botão (Presente na placa);
4. LED (Presente na placa).

**Cliente distribuído - Bateria**:
1. Dev Kit ESP32;
2. Botão (Presente na placa);
3. LED (Presente na placa).

**Broker MQTT** (Mosquitto - Núvem)

## 3. Conexões entre os módulos do sistema

1. A interconexão entre os dispositivos será toda realizada através do Protocolo MQTT via rede Ethernet/Wifi. 
2. A rede MQTT será coordenada por um Broker de acesso público (Ex: Mosquitto, Eclipse, HiveMQ, etc - https://mntolia.com/10-free-public-private-mqtt-brokers-for-testing-prototyping/);
3. Todas as mensagens via MQTT devem estar no formato JSON;
4. **ESP32**: o botão e o LED a serem usados são os dispositivos já integrados no próprio kit de desenvolimento (Botão = GPIO 0 / LED = GPIO 2);
5. **ESP32**: o sensor de temperatura/umidade a ser ligado à ESP32 será o DHT11 (Ligado à GPIO 4);
6. **Servidor Central**: o alarme de áudio deverá ser acionado pela saída de som do PC;

## 4. Requisitos

Abaixo estão detalhados os requisitos de cada um dos componentes do sistema.

#### **Servidor Central**:
1. O código do Servidor Central pode ser desenvolvido em qualquer linguagem de programação (Python, C/C++, JavaScript, etc), porém, o uso de qualquer framework irá exigir que a solução seja entregue dockerizada e ser executada através do Docker Compose;
2. Prover uma interface que mantenha o usuário atualizado sobre o estado de cada dispositivo (e suas respectivas entradas e saídas);
3. Prover mecanismo que identifique se o dispositivo está *online* ou se a conexão foi perdida (Este mecanismo pode ser implementado observando a regularidade de mensagens enviadas ou algum mecanismo de *ping*).
4. Para os dispositivos **ESP32-Energia**, as informações de atualização de entradas / saídas deve ocorrer imediatamente (seguindo só o atraso do fluxo de comunicação da rede). 
5. As informações do sensores de Temperatura/Umidade devem ser atualizadas a cada 10 segundos (O sensor deve ser lido a cada 2 segundos e o valor a ser enviado deve ser a média dos últimos 10 segundos);
6. Prover mecanismo para que o usuário possa acionar manualmente todos os dispositivos controláveis (lâmpadas, aparelhos de ar-condicionado, etc.);
7. Prover a opção de controlar a saída no modo dimerzável (Deve ser uma opção ao cadastrar o a saída do dispositivo);
8. Prover mecanismo para ligar e desligar o sistema de alarme que, quando ligado, deve tocar um som de alerta ao detectar presenças ou abertura de portas/janelas. Cada entrada, no momento do cadastro de um novo dispositivo deve prover um atributo a ser selecionado pelo usuário indicando se irá ou não acionar o alarme;
9. Prover suporte para adicionar e remover clientes ESP32 cujas funções do *botão* representarão entradas (sensores de presença/porta/janelas, interruptores, etc.) e do *led* representarão saídas (acionamento de lâmpadas, tomadas, ar-condicionado, etc.). Cada entrada/saída deve poder ser nomeada pelo usuário no momento da adição do dispositivo ESP32.\
A adição de um novo dispositivo será realizada assim que a ESP32 se conectar à rede Wifi e enviar uma mensagem inicial de configuração para o tópico `fse2021/<matricula>/dispositivos/<ID_do_dispositivo>`. Neste caso, na tela do Servidor Central deve aparecer o novo dispositivo a ser adicionado onde, através de um comando, deverá ser possível definir:
    1. O cômodo/local onde o dispositivo estará alocado e que irá definir o nome do tópico onde o mesmo irá publicar suas mensagens (Obs: o nome do tópico não pode conter espaços ou caracteres especiais - preferencialmente manter somente com letras (sem acentos) e números);
    2. O nome do dispositivo de Entrada e Saída sendo controlados pelo dispositivo; 
    3. Ao final da configuração, deve ser enviada uma mensagem à ESP32 informando o nome do *tópico* ao qual ela deverá, a partir deste momento, enviar a mensagem com o estado dos dispositivos que controla;
1. A remoção de um cliente pode ocorrer de 2 modos:  
   1. Através da interface gráfica onde o servidor central deverá informar ao dispositivo para que o mesmo volte às configurações de fábrica;
   2. Caso não haja comunicação com o dispositivo remoto, o servidor central deverá remover o dispositivo do cadastro e, na ESP32 deverá haver um modo de reset (pressionando o botão por 3 segundos).
2. Manter log (Em arqvuio CSV) dos comandos acionados pelos usuários e do acionamento dos alarmes;

**Observações**: 
1) A Matrícula a ser utilizada será a de um dos alunos da dupla de trabalho;  
2) O ID_do_dispositivo deve ser o MAC_ADRESS da ESP que pode ser obtido através de chamada de função.

#### **Cliente ESP32**:

Haverão duas configurações possíveis dos clientes ESP32. A **ESP32-Energia** irá funcionar conectada permanentemente à alimentação e a **ESP32-Bateria** representa um dispositivo operado por baterias (que deve funcionar em modo Low Power). **Atenção**! O firmware para as duas variações deverá ser o mesmo e a opção de funcionamento por um modo ou outro deverá ser definida através de uma variável de ambiente no ***menuconfig***.

1. O código da ESP32 deve ser desenvolvido em C utilizando o framework ESP-IDF (Deve ser indicado no README a versão do framework utilizada para o desenvolvimento e se foi feito usando a ESP-IDF nativa ou via PlatformIO);
2. A ESP32 deverá se conectar via Wifi (com as credenciais sendo definidas em variável de ambiente pelo Menuconfig);
3. Cada cliente ESP32, ao ser iniciado pela primeira vez, deve:  
    3.1 Enviar uma mensagem MQTT de inicialização para o tópico `fse2021/<matricula>/dispositivos/<ID_do_dispositivo>` e se inscrever no mesmo tópico. Esse será o canal de comunicação para envio de mensagens de configuração e também de retorno do servidor central.   
    3.2 Em seguida, o servidor central irá enviar uma mensagem de retorno (JSON) informando o nome do cômodo/local ao qual o dispositivo foi associado (que será o nome do tópico ao qual o mesmo irá publicar as informações sobre o estado de seus sensores). O formato deste tópico será: `fse2021/<matricula>/<cômodo>`.  
    A partir deste momento, cada mudança de estado nos dispositivos controlados pela ESP deve ser publicado nos seguintes tópicos:  
    ```
    fse2021/<matricula>/<cômodo>/temperatura
    fse2021/<matricula>/<cômodo>/umidade
    fse2021/<matricula>/<cômodo>/estado
    ```

4. Caso a ESP32 já tenha sido cadastrada no servidor central, a mesma deve guardar esta informação em sua memória não volátil (NVS) e, caso seja reiniciada, deve manter o estado anterior e não precisar se cadastrar novamente.
5. Realizar a leitura da temperatura e umidade à partir do sensor DHT11 a cada 2 segundos, calcular a média e enviar o resultado para o Servidor Central a cada 10 segundos;
6. Monitorar o botão utilizando interrupções e enviar por mensagem push a cada mudança do estado do botão;
7. Acionar o LED (Saída) à partir dos comandos enviados pelo servidor central. Caso a saída seja definida como dimerizável (Ex: lâmpada), sua intensidade poderá ser controlada à partir da técnica de PWM;

**Observação**: A versão da ESP32 operando por bateria deverá ter as mesmas características de comunicação descritas acima, porém, será utilizada exclusivamente para acionamento de sensores (entradas) operando em modo *low power* e enviando o estado de seu sensor via push sempre que houver uma mudança de estado. Neste caso, não haverá um sensor de temperatura / umidade acoplado.

## 5. README

A descrição de funcionamento bem como as instruções de como rodar todo o ambiente devem ser inscluidas no README dos repositórios (Servidor Centrar e ESP32).

Incluir um **vídeo** de até **10 minutos** demonstrando todo o funcionamento do sistema.

## 6. Critérios de Avaliação

A avaliação será realizada seguindo os seguintes critérios:

|   ITEM    |   DETALHE  |   VALOR   |
|-----------|------------|:---------:|
|**Servidor Central**    |       |       |
|**Interface (Estado)**  |   Interface apresentando o estado de cada dispositivo (entradas e saídas), temperaturas e umidades.  |   1,0   |
|**Interface (Acionamento)** |   Mecanismo para acionamento de dispositivos. |   0,5   |
|**Acionamento do Alarme**   |   Mecanismo de ligar/desligar alarme e acionamento do alarme de acordo com o estado dos sensores. |   0,5   |
|**Log (CSV)**   |   Geração de Log em arquivo CSV.  |   0,5 |
|**Clientes ESP32 - Energia**    |       |       |
|**Leitura de Temperatura / Umidade**    |   Leitura e envio dos valores médios de temperatura / umidade a cada 10 segundos.  |   1,0   |
|**Acionamento de Dispositivos** |   Correto acionamento e envio do estado da saída de acordo com os comandos do servidor Central.    |   0,5   |
|**Acionamento da Entrada** | Correta detecção e envio do estado da entrada ao servidor central.   |   0,5  |
|**Clientes ESP32 - Bateria**    |       |       |
|**Operação em modo Low Power** | Correta operação da placa em modo *low power* economizando energia.   |   1,0  |
|**Acionamento da Entrada** | Correta detecção e envio do estado da entrada ao servidor central sendo acordado somente no acionamento da GPIO em modo *low power*.   |   0,5  |
|**Geral**    |       |       |
|**Comunicação MQTT**  |   Correta implementação de comunicação entre os dispositivos. |   1,5   |
|**Mecanismo de Cadastramento de Clinetes ESP32**   |   Correta implementação do mecanismo de adição de clientes ESP32 tanto no servidor quanto a configuração do Cliente.  |   1,0 |
|**Qualidade do Código** |   Utilização de boas práticas como o uso de bons nomes, modularização e organização em geral. |   1,5 |
|**Pontuação Extra 1** |   Qualidade e usabilidade acima da média. |   0,5   |
|**Pontuação Extra 2** |   Implementação de mecanismo de fácil acesso para que o usuário possa fazer cadastrar as credenciais do Wifi na ESP32. |   0,5   |
|**Pontuação Extra 3** |   Suporte à OTA na ESP32. |   1,0   |


## 7. Referências

[Biblioteca DHT11 para ESP-IDF ESP32](https://github.com/0nism/esp32-DHT11)

[Eclipse Mosquitto - Broker MQTT](https://mosquitto.org/)
